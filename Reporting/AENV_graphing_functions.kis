// AENV_graphing_functions.kis
//
// Purpose: Hold functions used in producing AB Basin website figures, dashboard
//          plots and possibly other graphs
//
//
// Created Date : 2012/01/24
// Created By   : Brett Hammerlindl (CCG)
//
// Update History:
// CML 961  2012/01/24 Brett Hammerlindl (CCG)
//          Initial version
//          2012/02/22 Brett Hammerlindl
//          CML 1093 - handle gaps in timeseries
//          CML 1118 - treat negative values as bad data
//          CML 1130 - ignore data below 0.001 on logarithmic axis
//          2012/03/xx Brett Hammerlindl (CCG)
//          CML  961 add function that return a constant to clarify code
//          CML 1143 change color of axis to match axis label
// CML 1153 2012/06/12 Bob Beier (CCG)
//          Added 1 hour to now() function i.e. changed now() to (now() + 3600000).
// CML6101  2012/11/23 Bob Beier (CCG)
//          Backed out the now()+3600000 change and replaced it with a new nowDST() funciton.

// functions that return a constant

// functions for colors
function black(): Integer
  black = rgb(0,0,0)
endfunc

function blue(): Integer
  blue = rgb(0,0,255)
endfunc

function green(): Integer
  green = rgb(0,160,0)
endfunc

function gray(): Integer
  gray = rgb(80,80,80)
endfunc

function red(): Integer
  red = rgb(255,0,0)
endfunc

function orange(): Integer
  orange = rgb(255,128,0)
endfunc

function cyan(): Integer
  cyan = rgb(0,255,255)
endfunc

function magenta(): Integer
  magenta = rgb(255,0,255)
endfunc

// functions for axis type
function logAxis() : Integer
   logAxis = 2
endfunc

function regularAxis() : Integer
   regularAxis = 1
endfunc

// functions for display type on an axis
function barDisplay() : Integer
   barDisplay = 2
endfunc

function regularDisplay() : Integer
   regularDisplay = 1
endfunc

// function for no legend

function noLegend() : String
   noLegend = ""
endfunc

// functions for line width

function defaultLine() : Float
   defaultLine = 0.0
endfunc

function heavyLine() : Float
   heavyLine = 4.0
endfunc

function mediumLine() : Float
   mediumLine = 3.0
endfunc

function lightLine() : Float
   lightLine = 2.0
endfunc

// functions for line style

function solidLine() : Float
   solidLine = 0.0
endfunc

function smallDashes() : Float
   smallDashes = 2.0
endfunc

function mediumDashes() : Float
   mediumDashes = 4.0
endfunc

// structures shared between Web figures and Dashboard plots

// all information about a possible trace
struc PossibleTraceOption
  Boolean includePrevNext
  Boolean legendVisible
  Boolean sumTransformation
  Float allowedGapHours // maximum hours allowed for interpolation between data points
  Float lineWidth // 0.0 use default
  Float dash  // 0.0 use default
  Integer bufferArraySize  // initial size of bufferArray
  Integer color
  Integer monthOffset     // eg - use -12 for previous year
  Integer reverseMonthOffset //0 - monthOffset
  Integer qualityThreshold
  Long allowedGap // maximum milliseconds allowed for interpolation between data points
  String parameterName
  String timeseriesName
  String legendName
  Integer dataTransformation // 0 - no transformation, 1 - sum data, 2 - shift data so minimum value is 0.00
  Timestamp startTime
  Timestamp endTime
endstruc

struc PossibleTrace
  PossibleTraceOption[] optionArray
endstruc

// information about the traces on an Axis
struc ReportCallAxis
  Boolean barGraph
  PossibleTrace [] possibleTrace
  Float minimumAllowedValue // set based on on axisType
  Integer axisType // 1 for regular (number), 2 for log
  Integer displayType // 1 for regular (curve), 2 for bar
  String label
  Integer labelColor
endstruc

// call parameters and information derived from the call
struc ReportCall
  Integer xDimension
  Integer yDimension
  String directoryNamePattern
  ReportCallAxis [] leftAxis
  ReportCallAxis [] rightAxis
  String elevationAttrType
  String extraTables  // more tables to check in station reader
  String extraConditions // limit stations return by station reader
  String fileFormat
  String[] parameterChecks // entries are "= 'XX'" or "in ('P1','P2')"
  String reportType
  String titleLine2
  String titleLine3
  Timestamp startTime
  Timestamp endTime
endstruc

// a reader for a particular timeseries along with information about values already read
struc TimeSeriesReader
  Boolean moreData // is there more time series data to read
  Field valueField
  Float minValue
  Float maxValue
  Float runningSum
  Float value
  Float lastValue
  Integer nullValues
  Integer outOfRangeValues
  Integer poorQuality
  Integer recordsRead
  Integer validEntries
  String stationNo
  Timestamp lastTimestamp
  Timestamp valueTimestamp
  Reader reader
endstruc

// output to be added to a chart for the given option
struc VBufferArrayPlus
  Float minValue
  Float maxValue
  Integer arraySize
  PossibleTraceOption option
  ValueBuffer [] valueBuffer
endstruc

// everything to put on an Axis
struc DataSetPlus
  XYDataSet dataSet
  ReportCallAxis callAxis
  Integer annotatedLines
  Integer arraySize
  Float minRange
  Float maxRange
  VBufferArrayPlus[] bufferArray
endstruc

// information that is needed in nested calls - passed as a structure rather than as many parameters.
struc Context
  Integer stationId
  Reader stationRdr
  PossibleTraceOption option
  ReportCallAxis callAxis
  ReportCall call
  Resources resources
  String stationNo  
  String stationName
endstruc

function create (Reader stationRdr, ReportCall call, Resources resources): Context
  Context context = new Context
  
  context.stationRdr  = stationRdr
  context.stationId   = stationRdr.getField("id").getInteger()
  context.stationNo   = stationRdr.getField("no").getString()
  context.stationName = stationRdr.getField("name").getString()
  
  context.call        = call
  context.resources   = resources
  
  create = context
endfunc

function writeStationLog(String logMessage, Integer messageLevel, Context context) : Integer
  writeStationLog = writeLog(context.stationNo, logMessage, messageLevel, context.resources)
endfunc

// the remainder is code common to Web figures and Dashboard plots

// functions for any graph with logarithmic axis
function annotation(String format, Float value) : String
  if value = floor(value) then
    annotation = stri(floor(value))
  else 
    annotation = format(format, value)
  endif
endfunc

function pickFormat (Float value): String
  if value >= 0.1 then
    pickFormat = "%.1f"
  elseif value >= 0.01 then
    pickFormat = "%.2f"
  elseif value >= 0.001 then
    pickFormat = "%.3f"
  else
    pickFormat = "%.4f"
  endif
endfunc

function addMajorRangeLine(Chart chart, String format, Float value) : Integer
  // TODO raise CML - exception dash lengths all zero at call chart.saveChartAsXXXX
  // documentation says to use 0.0 for a solid line
  // error only comes up when using a logrithmic Axis
  chart.addAnnotationWithLine(value, annotation(format,value), true, black(), 2.0, 1.0)
endfunc

function addMajorRangeLine(Chart chart, Float value) : Integer
  addMajorRangeLine(chart, pickFormat(value), value)
endfunc

function addRangeLineGroup(Chart chart,  Float minValue, Integer annotatedLines) : Integer
  String format = pickFormat(minValue)
  addMajorRangeLine(chart, format, minValue)
  Float value = minValue
  Integer i
  for i = 2 to annotatedLines step 1
    value = value + minValue
    chart.addAnnotationWithLine(value, annotation(format,value), true, gray(), 1.0, 1.0)
  next
  for i = annotatedLines+1 to 9 step 1
    value = value + minValue
    chart.addRangeMarker(value, gray(), 1.0, 1.0)
  next
endfunc

// clarify code with "Constant functions" for commmon values for PossibleTraceOption structure

function threeHourGap(): Float
 threeHourGap = 3.0
endfunc

function oneDayGap(): Float
 oneDayGap = 24.0
endfunc

function ignoreDataGap(): Float
 ignoreDataGap = 0.0
endfunc

function stdArraySize(): Integer
  stdArraySize = 5
endfunc

function smallArraySize(): Integer
  smallArraySize = 1
endfunc

function largeArraySize(): Integer
  largeArraySize = 20
endfunc

// functions to set up options in the call structure.

function create (Boolean includePrevNext, String parameterName, String timeseriesName, String legendName, Integer dataTransformation, Integer monthOffset, Integer qualityThreshold, Integer color, Float lineWidth, Float dash, Float allowedGapHours, Integer bufferArraySize, ReportCall call)
  : PossibleTraceOption
  PossibleTraceOption option = new PossibleTraceOption
  option.includePrevNext     = includePrevNext
  option.parameterName       = parameterName
  option.timeseriesName      = timeseriesName
  option.legendName          = legendName
  option.dataTransformation  = dataTransformation
  option.monthOffset         = monthOffset
  option.qualityThreshold    = qualityThreshold
  option.color               = color
  option.lineWidth           = lineWidth
  option.dash                = dash
  option.allowedGapHours     = allowedGapHours
  option.bufferArraySize     = bufferArraySize
  // set up elements needed repeatedly
  option.allowedGap          = ceil(allowedGapHours*3600000)
  option.legendVisible       = not(legendName = noLegend())
  option.reverseMonthOffset  = 0 - monthOffset    
  option.startTime           = addMonth(call.startTime,option.monthOffset)
  option.endTime             = addMonth(call.endTime,option.monthOffset)
  option.sumTransformation   = (dataTransformation = sumData())
  create                     = option
endfunc

function create (PossibleTraceOption[] optionArray) : PossibleTrace
  PossibleTrace trace = new PossibleTrace
  trace.optionArray   = optionArray
  create              = trace
endfunc

function create2 (String parameterName, String timeseriesName, String legendName, Integer dataTransformation, Integer qualityThreshold, Integer color, Float lineWidth, Float dash, Float allowedGapHours, Integer bufferArraySize, ReportCall call)
  : PossibleTrace
  // typical case with aingle option for time series 
  PossibleTraceOption[] optionArray = new PossibleTraceOption[1]
  optionArray[0] = create(excludePrevNext(), parameterName, timeseriesName, legendName, dataTransformation, currentData(), &
                          qualityThreshold, color, lineWidth, dash, allowedGapHours, bufferArraySize, call)
  create2        = create(optionArray)
endfunc

function create (PossibleTrace [] possibleTrace, Integer axisType, Integer displayType, String label, Integer labelColor)
  : ReportCallAxis
  ReportCallAxis axis = new ReportCallAxis
  axis.possibleTrace  = possibleTrace
  axis.axisType       = axisType
  axis.displayType    = displayType
  axis.label          = label
  axis.labelColor     = labelColor
  // values needed repeatedly
  axis.barGraph = (displayType = barDisplay())
  if axisType = logAxis() then
    axis.minimumAllowedValue = 0.001
  else
    axis.minimumAllowedValue = 0.0
  endif 
  create              = axis
endfunc

function nextValidEntry(TimeSeriesReader timeSeriesReader, Context context): Boolean
  Field valueField
  Float minValue
  Float maxValue
  Float value
  Timestamp valueTimestamp
  PossibleTraceOption option = context.option
  Integer fieldCount = timeSeriesReader.reader.getFieldCount()  
  Integer i
  Boolean hasAbsValue = false
  for i = 0 to fieldCount-1 step 1
    if timeSeriesReader.reader.getField(i).getHeading() = "absvalue" then
      hasAbsValue = true
    endif
  next
  while timeSeriesReader.reader.next()
    timeSeriesReader.recordsRead = timeSeriesReader.recordsRead + 1
    // check entry is reasonable based on qualityThreshold
    valueTimestamp = timeSeriesReader.reader.getField("tstamp").getTimestamp()
    if isQualityData(timeSeriesReader.reader,option.qualityThreshold) then
      // if the absvalue field exists use it
      if hasAbsValue then
        valueField = timeSeriesReader.reader.getField("absvalue")
      else
        valueField = timeSeriesReader.reader.getField("value")
      endif
      if valueField.isNull() then
        // value missing
        timeSeriesReader.nullValues = timeSeriesReader.nullValues + 1
        writeStationLog("null value at " + formatTimestamp(valueTimestamp),15,context)
      else
        value = valueField.getFloat()
        if value < context.callAxis.minimumAllowedValue then
          // CML 1118 - value too low to graph             
          timeSeriesReader.outOfRangeValues = timeSeriesReader.outOfRangeValues + 1
          writeStationLog("found out of range value " + strf(value) + " at " + formatTimestamp(valueTimestamp),15,context)
        else
          // this is a good value
          timeSeriesReader.validEntries = timeSeriesReader.validEntries + 1
          timeSeriesReader.lastTimestamp = timeSeriesReader.valueTimestamp
          timeSeriesReader.lastValue = timeSeriesReader.value
          if option.reverseMonthOffset = 0 then
            timeSeriesReader.valueTimestamp = valueTimestamp
          else
            timeSeriesReader.valueTimestamp = addMonth(valueTimestamp,option.reverseMonthOffset)
          endif
          // handle the sum dataTransformation
          if option.sumTransformation then
            value = timeSeriesReader.runningSum + value
            timeSeriesReader.runningSum =  value
          endif
          timeSeriesReader.value = value
          // update data reader min and max value if appropriate
          if timeSeriesReader.maxValue < value then
            timeSeriesReader.maxValue = value
          elseif timeSeriesReader.minValue > value then
            timeSeriesReader.minValue = value
          endif
          nextValidEntry = true
          return
        endif
      endif
    else //isQualityData
      // value failed quality test
      timeSeriesReader.poorQuality = timeSeriesReader.poorQuality + 1
      writeStationLog("omitting poor quality value at " + formatTimestamp(valueTimestamp),15,context)
    endif //isQualityData
  endwhile  // timeSeriesReader.next()
  // no more timeseries data
  timeSeriesReader.reader.close()
  timeSeriesReader.lastTimestamp = timeSeriesReader.valueTimestamp
  timeSeriesReader.lastValue = timeSeriesReader.value
  timeSeriesReader.valueTimestamp = null
  timeSeriesReader.value = null
  nextValidEntry = false
endfunc

function create (Integer tsId, Context context)
  : TimeSeriesReader
  PossibleTraceOption option = context.option
  TimeSeriesReader timeSeriesReader = new TimeSeriesReader
  timeSeriesReader.runningSum = 0.0
  timeSeriesReader.minValue   = 0.0
  timeSeriesReader.maxValue   = 0.0
  timeSeriesReader.nullValues       = 0
  timeSeriesReader.outOfRangeValues = 0
  timeSeriesReader.poorQuality      = 0
  timeSeriesReader.validEntries     = 0
  timeSeriesReader.recordsRead      = 0
  timeSeriesReader.valueTimestamp = option.startTime
  timeSeriesReader.value  = null
  timeSeriesReader.reader = context.resources.tsmConn.getTsData(tsId,option.startTime,option.endTime, option.includePrevNext, false, 0) 
  // look for first record
  timeSeriesReader.moreData = nextValidEntry(timeSeriesReader, context)
  // initialize minimum and maximum values
  if option.sumTransformation then
    timeSeriesReader.minValue = 0
  else
    timeSeriesReader.minValue = timeSeriesReader.value
  endif
  timeSeriesReader.maxValue = timeSeriesReader.value
  // need something in lastTimestamp and lastValue
  timeSeriesReader.lastTimestamp = timeSeriesReader.valueTimestamp
  timeSeriesReader.lastValue = timeSeriesReader.value
  create = timeSeriesReader
endfunc

function handleInterval(TimeSeriesReader timeSeriesReader, Context context) : ValueBuffer
  ValueBuffer buffer = new ValueBuffer ()
  Long allowedGap = context.option.allowedGap
  Boolean barGraph = context.callAxis.barGraph
  Boolean checkGap = (allowedGap > 0 and not barGraph)
  Boolean gapFound = false
  Field timeVB  = buffer.createTimestampField("Time")
  Field valueVB = buffer.createFloatField("Value")
  Integer validEntries = 0
  Long milliseconds
  String message
  Timestamp firstTimestamp = timeSeriesReader.valueTimestamp
  // open value buffer and record values
  buffer.open()
  while timeSeriesReader.moreData and not gapFound
    // write value to buffer unless a zero value for a bar graph
    if timeSeriesReader.value > 0.0 or validEntries = 0 or not barGraph then
      timeVB.setTimestamp(timeSeriesReader.valueTimestamp)
      valueVB.setFloat(timeSeriesReader.value)
      buffer.append()
    else 
      writeStationLog("skipping zero value on bar graph at " + formatTimestamp(timeSeriesReader.valueTimestamp),20,context)
    endif
    validEntries = validEntries + 1
    timeSeriesReader.moreData = nextValidEntry(timeSeriesReader, context)
    if timeSeriesReader.moreData then
      if checkGap then
        // see if we have found a gap that is larger than the allowed gap
        milliseconds = intervalMilliseconds(timeSeriesReader.lastTimestamp,timeSeriesReader.valueTimestamp)
        gapFound = milliseconds > allowedGap
      endif
    endif
  endwhile
  // report the interval unless it is a single point not on a bar graph
  if validEntries > 1 then
    message =  "Reporting interval " + formatTimestamp(firstTimestamp) + " to " + formatTimestamp(timeSeriesReader.lastTimestamp) + " - "+ formatIntervalLength(firstTimestamp, timeSeriesReader.lastTimestamp)
    handleInterval = buffer
  elseif barGraph then
    message =  "Reporting single value at " + formatTimestamp(timeSeriesReader.lastTimestamp)
    handleInterval = buffer
  else
    // no point in writing buffer with a single element (except for a bar graph)
    message =  "Skipping single value at " + formatTimestamp(firstTimestamp)
    handleInterval = null
  endif
  if gapFound then
    // report the gap
    message = message + " followed by a gap of " + formatIntervalLength(milliseconds)
  endif
  writeStationLog(message,5,context)
endfunc

function adjustFieldInVB (ValueBuffer valueBuffer, Field field, Float adjustment) : Integer
  // reduce the value in field by the adjustment in each row of the valueBuffer
  Float value
  valueBuffer.goTop()
  while not valueBuffer.eovb()
    value = field.getFloat()
    field.setFloat(value + adjustment)
    valueBuffer.update()
    valueBuffer.skip(1)
  endwhile
endfunc //adjustFieldInVB


function adjustValueBufferArray (ValueBuffer [] vbArray, Integer arraySize, Float adjustment)
  : Integer
  // adjust the value of each row in each ValueBuffer by adjustment
  Integer i  
  Field field
  ValueBuffer valueBuffer
  for i = 0 to (arraySize - 1) step 1
    valueBuffer = vbArray[i]
    field = valueBuffer.getField("value")
    adjustFieldInVB (valueBuffer, field, adjustment)
  next
endfunc

function increaseArraySize(ValueBuffer[] bufferArray) : ValueBuffer[]
  Integer entries = bufferArray.length
  Integer newSize 
  if entries >= largeArraySize() then
    newSize = entries + largeArraySize()
  elseif entries >= stdArraySize() then
    newSize = largeArraySize()
  else
    newSize = stdArraySize()
  endif
  ValueBuffer [] newArray = new ValueBuffer[newSize]
  Integer i
  for i = 0 to (entries -1 ) step 1
    newArray[i] = bufferArray [i]
  next
  increaseArraySize = newArray
endfunc
            
function create (PossibleTraceOption option, Context context)
  : VBufferArrayPlus
  context.option = option
  Resources resources = context.resources
  create = null
  Integer parTcaId
  Integer parTsmId
  Integer tsId
  Reader parRdr
  Reader parTsmRdr
  Reader tsRdr
  String logMessage
  Integer messageLevel = 4
  String parameterName  = option.parameterName
  String timeseriesName = option.timeseriesName
  ValueBuffer [] valueBuffer = new ValueBuffer [option.bufferArraySize]
  VBufferArrayPlus bufferArray = new VBufferArrayPlus 
  
   // is the time series defined for this station and data present
  parRdr = cfg.getParameterInfo("station = " + context.stationId + " and typename = '" + parameterName + "'", "")
  if parRdr.next() then
    parTcaId = parRdr.getField("id").getInteger()
    parTsmRdr = resources.tsmConn.getBasicDataList("tcaRefKey = " + parTcaId, "")
    if parTsmRdr.next() then
      // check the necessary time series defined
      parTsmId = parTsmRdr.getField("id").getLong()
      tsRdr = resources.tsmConn.getTimeseriesList("name = '" + timeseriesName + "' and basicDataId = " + parTsmId, "")
      if tsRdr.next() then
        // setup a reader for this timeseries and the specified date range
        // occasionaly adjust the start and end time to the previous year
        tsId = tsRdr.getField("id").getLong()
        String source = "Time series "+ timeseriesName + " of parameter " + parameterName
        if not (option.monthOffset = 0) then 
          source = source + " for interval " + formatTimestamp(option.startTime) + " to " + formatTimestamp(option.endTime)
        endif
        TimeSeriesReader timeSeriesReader = create (tsId, context)
        // read the time series data into a value buffer
        Integer arraySize = 0
        ValueBuffer buffer
        while timeSeriesReader.moreData
          buffer = handleInterval(timeSeriesReader, context)
          if not (buffer = null) then
            if arraySize >= valueBuffer.length then
              // add more entries to value buffer
              valueBuffer = increaseArraySize(valueBuffer)
              writeStationLog("Array size increased to " + stri(valueBuffer.length),5,context)
            endif
            valueBuffer [arraySize] = buffer
            arraySize = arraySize + 1
          endif
        endwhile
        // confirm that some reasonable data found
        if arraySize = 0 then
          logMessage = "No Valid Data for " + source
        else
          // handle the transformation to reduce minimum value to zero
          Float minValue = timeSeriesReader.minValue
          Float maxValue = timeSeriesReader.maxValue
          if option.dataTransformation = shiftForZeroMinimum() and minValue > 0 then
            Float adjustment = 0.0 - minValue
            minValue = 0.0
            maxValue = maxValue + adjustment
            adjustValueBufferArray(valueBuffer,arraySize,adjustment)
            source = source + " adjusted by "+ strf(adjustment) + " to have zero minimum"
          endif
          logMessage =  source + " has " + arraySize + " segments with " &
                      + timeSeriesReader.validEntries + " valid entries, " & 
                      + timeSeriesReader.poorQuality + " poor quality entries, " &
                      + timeSeriesReader.nullValues + " null values and " &
                      + timeSeriesReader.outOfRangeValues + " values out of range " 
          messageLevel = 1
          // complete the VBufferArrayPlus
          bufferArray.minValue = minValue
          bufferArray.maxValue = maxValue
          bufferArray.arraySize = arraySize
          bufferArray.option = option
          bufferArray.valueBuffer = valueBuffer
          create = bufferArray
        endif
      else
        logMessage = "Parameter " + parameterName + " does not have time series " + timeseriesName + " defined for this station"
      endif // tsRdr.next()
      tsRdr.close()
    else
      logMessage = "getBasicDataList (tcaRefKey=" + strl(parTcaId) + ",null) failed. Parameter Name is " + parameterName
    endif //parTsmRdr.next()
    parTsmRdr.close()
  else
    logMessage = "Parameter " + parameterName + " is not defined for this station"
  endif // parRdr.next()
  parRdr.close()
  writeStationLog(logMessage,messageLevel,context)
endfunc // create : VBufferArrayPlus

function create (PossibleTrace possibleTrace, Context context)
  : VBufferArrayPlus
  VBufferArrayPlus bufferArray
  PossibleTraceOption option
  Integer i
  for i = 0 to (possibleTrace.optionArray.length - 1) step 1
    option = possibleTrace.optionArray[i]
    writeStationLog("Checking option " + option.parameterName + "/" + option.timeseriesName,10,context)
    bufferArray = create (option, context)
    if not (bufferArray = null) then
      create = bufferArray
      return
    endif
  next
  create = null
endfunc // create : VBufferArrayPlus

function closeValueBuffers (VBufferArrayPlus bufferArray) : Integer
  Integer i
  for i = 0 to (bufferArray.arraySize - 1) step 1
    bufferArray.valueBuffer[i].close()
  next
endfunc

function create(ReportCallAxis callAxis, Context context)
  : DataSetPlus
  context.callAxis = callAxis
  VBufferArrayPlus bufferArray
  VBufferArrayPlus [] bufferArrayArray = new VBufferArrayPlus[callAxis.possibleTrace.length]
  XYDataSet dataSet = new XYDataSet()
  Integer arraySize = 0
  ValueBuffer buffer
  String legendName
  XYTrace trace
  Integer i
  Integer j
  for i = 0 to (callAxis.possibleTrace.length - 1) step 1
    bufferArray = create (callAxis.possibleTrace[i], context)
    if not (bufferArray = null) then
      // save each bufferArray returned and build traces
      bufferArrayArray[arraySize] = bufferArray
      arraySize = arraySize + 1
      legendName = bufferArray.option.legendName
      for j = 0 to (bufferArray.arraySize - 1) step 1
        buffer = bufferArray.valueBuffer[j]
        trace = new XYTrace(buffer, buffer.getField("Time"), buffer.getField("Value"), legendName)
        dataSet.add(trace)
      next
    endif
  next
  // exit when no data found
  if arraySize = 0 then
    create = null
    return
  endif
  // build dataSetplus
  DataSetPlus dataSetPlus = new DataSetPlus
  dataSetPlus.dataSet = dataSet
  dataSetPlus.callAxis = callAxis
  dataSetPlus.bufferArray = bufferArrayArray
  dataSetPlus.arraySize = arraySize
  Float minRange = bufferArrayArray[0].minValue
  Float maxRange = bufferArrayArray[0].maxValue
  Integer annotatedLines = 0
  for i = 1 to arraySize-1 step 1
    if minRange > bufferArrayArray[i].minValue then
      minRange = bufferArrayArray[i].minValue
    endif
    if maxRange < bufferArrayArray[i].maxValue then
      maxRange = bufferArrayArray[i].maxValue
    endif
  next
  if callAxis.axisType = logAxis() then
    // adjust maximum of range based on maximum value
    if maxRange <= 1.0 then
      maxRange = 1.0
    elseif maxRange <= 10.0 then
      maxRange = 10.0
    elseif maxRange <= 100.0 then
      maxRange = 100.0
    elseif maxRange <= 1000.0 then
      maxRange = 1000.0
    elseif maxRange <= 10000.0 then
      maxRange = 10000.0
    else
      maxRange = 100000.0
    endif
    // adjust minimum of range based on minimum value
    if minRange >= maxRange * 0.01 then
      minRange = maxRange * 0.01
      annotatedLines = 9
    elseif minRange >= maxRange * 0.001 then
      minRange = maxRange * 0.001
      annotatedLines = 7
    else
      minRange = maxRange * 0.0001
      annotatedLines = 6
    endif
  else // not axisType = logAxis()
    // set range to be at least a unit
    if maxRange > 1.0 then
      maxRange = ceil(maxRange)
    else
      maxRange = 1.0
    endif
    if minRange > 1.0 then
      minRange = floor(minRange)
    else
      // get rid of all negative values
      minRange = 0.0
    endif
  endif
  dataSetPlus.annotatedLines = annotatedLines
  dataSetPlus.minRange = minRange
  dataSetPlus.maxRange = maxRange
  create = dataSetPlus
endfunc //create : DataSetPlus

function create(ReportCallAxis [] callAxis, Context context)
  : DataSetPlus
  if callAxis = null then
    create = null
    return
  endif
  DataSetPlus dataSetPlus 
  Integer i
  // look through Axis options and exit on first one with data
  for i = 0 to (callAxis.length - 1) step 1
    dataSetPlus = create (callAxis[i], context)
    if not (dataSetPlus = null) then
      create = dataSetPlus
      return
    endif
  next 
  create = dataSetPlus
endfunc //create : DataSetPlus

function setTraceProperties(Chart chart, VBufferArrayPlus bufferArray, Integer rendererIndex, Integer firstSeriesIndex): Integer
  PossibleTraceOption option = bufferArray.option
  Boolean legendVisible = option.legendVisible
  Integer color         = option.color
  Float  lineWidth      = option.lineWidth
  Float  dash           = option.dash
  // set properties for each trace associated with buffer array
  Integer seriesIndex
  Integer lastSeriesIndex = firstSeriesIndex + bufferArray.arraySize - 1
  for seriesIndex = firstSeriesIndex to lastSeriesIndex step 1
    chart.setXYTraceColor(rendererIndex, seriesIndex, color)
    if lineWidth > 0.0 then  
      chart.setXYTraceStroke(rendererIndex, seriesIndex, lineWidth, dash)
    endif
    // include trace name in legend if option say to do that
    chart.setLegend (rendererIndex, seriesIndex, legendVisible)
    // prevent repeat of trace name in legend when many traces for one option
    legendVisible = false
  next
endfunc

function plotOneRangeAxis(DataSetPlus dataSetPlus, Chart chart, Integer axisIndex, Integer dataSetIndex, Integer rendererIndex, TextAnchor location) : Axis
    ReportCallAxis callAxis = dataSetPlus.callAxis
    Axis axis
    Integer i
    chart.setXYAreaRenderer (rendererIndex,2)
    chart.addDataSet(dataSetIndex,dataSetPlus.dataSet)
    if callAxis.axisType = logAxis() then
      axis = chart.createLogAxis(callAxis.label, false, true)
      axis.setTickLabelsVisible(false)
      axis.setTickMarksVisible (false)
    else 
      axis = chart.createNumberAxis(callAxis.label, false)
    endif 
    axis.setRange(dataSetPlus.minRange,dataSetPlus.maxRange)
    axis.setLocation(location)
    axis.setLabelFont("Arial",callAxis.labelColor, 15)
    // CML 1143 - next two lines
    axis.setTickLabelPaint(callAxis.labelColor)
    axis.setAxisLinePaint(callAxis.labelColor) 
    chart.setRangeAxis(axisIndex, axis)
    chart.mapDatasetToRangeAxis(dataSetIndex, axisIndex)
    if callAxis.barGraph then
      chart.setXYBarRenderer (rendererIndex,0.0,true,false,false,true)
      chart.convertDataToBarData(dataSetIndex, 10.0)
    endif
    //add colors, line widths and dash for each trace
    Integer seriesIndex = 0
    VBufferArrayPlus bufferArray
    for i = 0 to (dataSetPlus.arraySize - 1) step 1
      bufferArray = dataSetPlus.bufferArray[i]
      setTraceProperties(chart, bufferArray, rendererIndex, seriesIndex)
      seriesIndex = seriesIndex + bufferArray.arraySize
    next
    
    // add range lines if axis type is logarithmic and first axis   
    if callAxis.axisType = logAxis() and axisIndex = 0 then
      Float range    = dataSetPlus.minRange
      while (range <  dataSetPlus.maxRange)
        addRangeLineGroup(chart, range, dataSetPlus.annotatedLines)
        range = range * 10.0
      endwhile
      addMajorRangeLine(chart, dataSetPlus.maxRange)
    endif 

    plotOneRangeAxis = axis
endfunc // plotOneRangeAxis

function plotOneGraph (Reader stationRdr, String directoryName, ReportCall call, Resources resources): Boolean
  plotOneGraph = false
  // setup the context
  Context context = create(stationRdr, call, resources)
  
  String logMessage  = "Start graph for station " + context.stationName +  "/" + strl(context.stationId) 
  writeStationLog(logMessage, 1, context)
  
  // read the timeseries and get the data
  DataSetPlus leftDataSetPlus  = create(call.leftAxis,  context)
  DataSetPlus rightDataSetPlus = create(call.rightAxis, context)

  Chart chart = new Chart()
  
  // titles for top of figure
  // report elevation if it is present and desired
  String elevationString = ""
  Reader xStationRdr = cfg.getExtraStationInfo("id = "+ context.stationId +" and attrtype = '" + call.elevationAttrType +"' ", "")
  if xStationRdr.next() then
    Float value = xStationRdr.getField("attrvalue").getFloat()
    elevationString = " Elevation:" + strf(value) + "m"
  endif
  xStationRdr.close()
  String title
  title = context.stationName + " (" + context.stationNo + elevationString + ")"
  title = title + chr(13) + chr(10) + call.titleLine2
  if not(call.titleLine3 = null) then
    title = title + chr(13) + chr(10) + call.titleLine3
  endif
  
  chart.setTitle(title)
  // titles for bottom of figure
  String latitude  = strf(round(stationRdr.getField("latitude" ).getFloat(),5))
  String longitude = strf(round(stationRdr.getField("longitude").getFloat(),5))  
  Timestamp rptCreated
  if resources.inDST = true then
    rptCreated = now()+3600000
  else
    rptCreated = now()
  endif
    
  String centerTitle = "Longitude:" + longitude +  " Latitude:" + latitude + "   Created: " + formatTimestamp(rptCreated, resources.timezone)
  chart.addBottomTitle("Government of Alberta", "left", "Arial", 14, black())
  chart.addBottomTitle(centerTitle, "center", "Arial", 14, black())
  chart.addBottomTitle("*Preliminary Data subject to Revision ", "right", "Arial", 14, black())  
  chart.setLegendPosition("TOP")
  
  // Domain Axis is always dates
  Axis dateAxis = chart.createDateAxis("Date (MM/dd)", false)
  dateAxis.setLabelFont("Arial",black(), 15)
  dateAxis.setRange(call.startTime.asLong(), call.endTime.asLong())
  Integer days = (call.endTime - call.startTime) / 1000L / 86400.0
  if days > 100 then
    //report in months
    dateAxis.setDateTickUnits(1, 1, "MMM")
    dateAxis.setLabel("Month")
  else
    // report in days or weeks
    Integer count
    if days > 20 then
      count =  7
    else
      count = 1
    endif
    dateAxis.setDateTickUnits(2, count, "MM/dd")
  endif
  
  chart.setDomainAxis(0, dateAxis)
  
  Integer axisIndex = -1
  Integer dataSetIndex = -1
  Integer rendererIndex = -1
  // Although most graphs have a left axis, Fires stations are an exception.
  if leftDataSetPlus = null then
    // handle the situation where there is no data for the requested period
    if rightDataSetPlus = null then
      writeStationLog("No data for requested period", 5, context)
      chart.setNoDataMessage("Requested data not found for this period.") 
    endif
  else
    // add left range axis when there is a left Dataset
    axisIndex = axisIndex + 1
    dataSetIndex = dataSetIndex + 1
    rendererIndex = rendererIndex + 1
    Axis leftAxis = plotOneRangeAxis(leftDataSetPlus, chart, axisIndex, dataSetIndex, rendererIndex, BOTTOM_LEFT)
    //writeStationLog("Left Range axis complete")
  endif

  if not (rightDataSetPlus = null) then
    // add right range axis when there is a right Dataset
    // eg - Meteorological has right axis for accumulated precipation in period
    axisIndex = axisIndex + 1
    dataSetIndex = dataSetIndex + 1
    rendererIndex = rendererIndex + 1
    Axis rightAxis = plotOneRangeAxis(rightDataSetPlus, chart, axisIndex, dataSetIndex, rendererIndex, BOTTOM_RIGHT)
    //writeStationLog("Right Range axis complete")
  endif
  
  // write the plot to the file system on the server
  String fileName = context.stationNo + "." + call.fileFormat
  if left(call.reportType,9) = "Dashboard" then
    fileName = context.stationName + "." + fileName
  endif
  if call.fileFormat = "jpeg" then
    chart.saveChartAsJPEG(directoryName, fileName, call.xDimension, call.yDimension)
  elseif call.fileFormat = "pdf" then
  
    chart.saveChartAsPDF (directoryName + "/" + fileName, call.xDimension, call.yDimension)
  else
    chart.saveChartAsPNG (directoryName, fileName, call.xDimension, call.yDimension)
  endif
  writeStationLog("Graph for " + context.stationName + " on server in " + directoryName + "\"  + fileName, 0, context)
      
  // free up resources no longer needed
  Integer i
  //  close the Value Buffer for each graphed Time Series
  if not (leftDataSetPlus = null) then
    for i = 0 to (leftDataSetPlus.arraySize - 1) step 1
      closeValueBuffers(leftDataSetPlus.bufferArray[i])
    next
  endif
  if not (rightDataSetPlus = null) then
    for i = 0 to (rightDataSetPlus.arraySize - 1) step 1
      closeValueBuffers(rightDataSetPlus.bufferArray[i])
    next
  endif

  plotOneGraph=true
endfunc //plotOneGraph
